from threading import Thread

import time

def helloThread(name):
    time.sleep(2)
    print('%s Hello ' %name)

if __name__ == '__main__':
    t = Thread(target=helloThread,args=('one',)) # 这里后面要加一个','应为他是一个元组
    t.start()
    print("主线程")