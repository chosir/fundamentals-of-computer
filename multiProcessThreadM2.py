"""
title:一个进程下开启多个线程与一个进程下开启多个进程的区别
"""
from threading import Thread
from multiprocessing import Process
import os

def work():
    print('Hello',os.getpid())

if __name__ == '__main__':
    # 在主进程下开启多个线程，每个线程都跟主进程pid一样
    t1 = Thread(target=work)
    t2 = Thread(target=work)
    t1.start()
    t2.start()
    print('主线程/主进程pid',os.getpid())

    # 主进程下开多个进程，每个进程都有不同的pid
    p1 = Process(target=work)
    p2 = Process(target=work)
    p1.start()
    p2.start()

    print('主进程/主进程pid',os.getpid())