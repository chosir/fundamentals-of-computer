"""
title: 主线程下的子线程
date: 20190816
auther: rightsec
"""
from threading import Thread
import threading
from multiprocessing import Process
import os
def work():
    import time
    time.sleep(3)
    print(threading.current_thread().getName)

if __name__ == '__main__':
    # 主进程下开启线程
    t = Thread(target=work)
    t.start()

    print(threading.current_thread().getName)
    print(threading.current_thread()) # 主线程

    print(threading.enumerate()) # 连同主线程在内有两个运行的线程

    print(threading.active_count()) # 返回当前正在运行线程数量
    print('主线程/主进程')