"""
title: 同一进程下，线程共享进程数据？
date: 20190816
auther: rightsec
"""
from threading import Thread
from multiprocessing import Process
import os

def work():
    global n
    n=0
    print('work n值',n)

if __name__ == '__main__':
    # n = 2
    # p = Process(target=work)
    # p.start()
    # p.join()
    # print('主进程',n) # 子进程p已经将自己的全局n改成了0，仅仅是改了自己的，父进程的n仍然为100

    n = 1
    t = Thread(target=work)
    t.start()
    t.join()
    print('主进程',n) # n为0,因为同一进程内的线程共享进程内的数据