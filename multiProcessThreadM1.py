"""
title: 进程下开启线程和进程的区别
date: 20190816
auther: rightsec
"""

from threading import Thread
from multiprocessing import Process
import os

def work():
    print('Hello')

if __name__ == '__main__':
    # 在主进程下开启线程
    t = Thread(target=work)
    t.start()
    print('主线程/主进程')

    # 在主进程下开启子进程
    t=Process(target=work)
    t.start()
    print('主线程/主进程')
    